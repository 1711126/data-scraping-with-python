import datetime
from os import write
import requests

from bs4 import BeautifulSoup

URL = "https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/Press-Release"
r = requests.get(URL)


soup = BeautifulSoup(r.content, 'html5lib')


table = soup.find("table")
print(table)

tablerow = table.find_all("tr")


ct = datetime.datetime.now()
ts = ct.timestamp()

url = URL.split("/")

name = str(url[2]) + "_" + str(ts) + "_" + "1711126"
with open(f"{name}.csv", "w", encoding="UTF-8") as f:
 
    for tr in tablerow:
        tabledata = tr.find_all("td")
        f.write(str(tabledata[0].contents[0]) + ",")
        f.write(str(tabledata[1].contents[1].contents[0])+",")
        f.write(str(tabledata[2].contents[0]) + "\n")
